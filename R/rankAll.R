rankAll <- function(yfactor, num_sets, ntrees=1000, outdir='../output'){
# Computes ranks for all of the features in a set that was saved from 
# within Ocatve using bunchOfWts or bunchPlus by calling rankFeatures
# on each of them.
#
# Args:
#   yfactor: the labels, as a factor
#   num_sets: the number of sets to rank
#   ntrees: (default 1000) Passes to rankFeatures. The number of trees
#      randomForest should use.
#   outdir: the directory to write the ranks back to.
#
# Return:
#   none, but writes the ranks of the kth feature set to a file with
#      name OUTPATH<k>.out

  source("rankFeatures.R")
  INPATH <- "features"
  OUTPATH <- paste(outdir, "/ranks", sep="")
  EXT <- ".out"
  for (k in 1:num_sets){
    infile <- paste(INPATH, k, EXT, sep="")
    outfile <- paste(OUTPATH, k, EXT, sep="")
    rankFeatures(yfactor, ntrees=ntrees, infile=infile, outfile=outfile)
  }
}