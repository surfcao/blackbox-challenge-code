rankFeatures <- function(yfactor, data = c(), ntrees=1000,
                         infile = c(), outfile = c()){
# Computes the ranking of a set of features using R's randomForest package.
# Writes the result in best-first order in a file that Octave can read.
#
# NB: if data is provided, infile will not be read
#
# Args:
#   yfactor: the labels, as a factor
#   data: Optional data matrix.
#   infile: Optional. File containing x only. 
#            Save with default or -text in Octave.
#   outfile: the file or path to write the rank vector to  
#   ntrees: (default 1000) Passed to randomForest. 
#           The number of trees to use in the randomForest used to assess
#           variable importance.
#
# Returns:
#   a vector with the indices of the features, ranked by variable importance
#
# NB: also writes the output to outfile if provided

  source('read1octave.R')
  source('write1octave.R')
  library(randomForest)

  if(length(data) == 0){
    data <- read1octave(infile)
  }
  rf <- randomForest(data, yfactor, importance=TRUE, ntree=ntrees)
  # This is supposed to sort by mean decrease in accuracy,
  # which should be one past the end of the per-class fields.
  sort.column <- length(unique(yfactor)) + 1
  imp <- sort.int(rf$importance[,sort.column], index.return=TRUE, 
                               decreasing=TRUE)
  if(length(outfile) != 0){
    write1octave(imp$ix, outfile)
  }
  imp$ix
}