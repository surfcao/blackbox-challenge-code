read1octave <- function(filename){
# Reads one vector or matrix from Octave that has been saved with save -text,
# which is the default.
#
# Arg:
#   filename: the name of a file with one vector or matrix in it
#
# Return:
#   the vector or matrix as a numeric or matrix type object

  library(foreign)
  data <- read.octave(filename)
  read1octave <- data[[1]]
}